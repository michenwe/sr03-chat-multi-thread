import java.net.Socket;
import java.io.IOException;
import java.io.DataOutputStream;
import java.net.SocketException;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.Scanner;

/**
 * Cette classe est pour l'objectif de récupère cu que le client tape et envoye via socket au serveur
 * Elle contient un socket qui est le channel de communication entre le client et le serveur
 */
public class MessageSender extends Thread {
    private Socket client;
    private DataOutputStream out;
    public MessageSender(Socket client) throws IOException {
        this.client = client;
        this.out = new DataOutputStream(client.getOutputStream());
    }
    @Override
    public void run(){
        while (!client.isClosed()) {
            try{
                System.out.println("Message Sender established"); /*connection established!*/
                Scanner sc = new Scanner(System.in);
                // chaque entréé par client est délimité par \n
                sc.useDelimiter("\n");
                String msg,pseudo;

                System.out.println ("Entrer votre pseudo: ");
                pseudo = sc.nextLine();
                out.writeUTF(pseudo);

                while (true){
                    msg = sc.nextLine();
                    if (msg.equals("exit")){
                        //System.out.println(pseudo + " exit");
                        out.writeUTF(msg);
                        disconnection();
                        System.out.println("Client sender end");
                    }
                    out.writeUTF(msg);
                }
            } catch(SocketException socketException) {
                System.out.println("Vous etes bien déconnecté !");
            } catch (IOException ex) {
                Logger.getLogger(ClientChat.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    /**
     *treat the offline of the client
     * @throws IOException
     */
    private void disconnection() throws IOException {
        out.close();
        client.shutdownOutput();
    }

}
