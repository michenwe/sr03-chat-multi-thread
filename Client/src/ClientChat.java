import java.net.Socket;
import java.io.IOException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Cette classe est le manager du cote client
 * Elle contient un threadpool pour bien organiser les deux threads msgsender et msgreceptor
 */
public class ClientChat {
    public static boolean connectionState = false;

    public boolean exit = false;

    // 创建一个大小为3的线程池
    static ExecutorService executor = Executors.newFixedThreadPool(3);
    public static void main(String[] args) {
        ClientChat clientChat = new ClientChat();
        while (!connectionState) {
            connect(clientChat);
            try {
                Thread.sleep(3000);
            }catch (Exception ex){
                Logger.getLogger(ClientChat.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    private static void connect(ClientChat clientChat){
        try {
            Socket client = new Socket ("localhost", 20001);
            connectionState = true;
            // 将两个Thread类中的任务包装成Runnable对象，然后提交到线程池中
            MessageSender msgsender = new MessageSender(client);
            executor.submit(msgsender);
            //msgsender.start();
            MessageReceptor msgreceptor = new MessageReceptor(client,clientChat);
            //msgreceptor.start();
            executor.submit(msgreceptor);
            ClientHeart clientheart = new ClientHeart(client,clientChat);
            //clientheart.start();
            executor.submit(clientheart);
            executor.shutdown();
            // 初始化事件函数

        } catch (IOException ex) {
            //Logger.getLogger(ClientChat.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("Pas de connexion au serveur.....");
            connectionState = false;
        }
    }
    public static void reconnect(ClientChat clientChat){
        while (!connectionState){
            System.out.println("Essayer de reconnecter le serveur.....");
            connect(clientChat);
            try {
                Thread.sleep(3000);
            }catch (Exception ex){
                Logger.getLogger(ClientChat.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}