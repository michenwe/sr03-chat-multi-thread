import java.net.Socket;
import java.io.IOException;
import java.io.DataInputStream;
import java.net.SocketException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Cette classe est pour l'objectif d'intercepter le msg envoyé par le serveur via socket
 * Elle contient un socket qui est le channel de communication entre le client et le serveur
 */
public class MessageReceptor extends Thread {
    private Socket client;
    private ClientChat clientChat;
    private DataInputStream in;
    public MessageReceptor(Socket client, ClientChat clientChat) throws IOException {
        this.client = client;
        this.clientChat = clientChat;
        this.in = new DataInputStream(client.getInputStream());
    }
    @Override
    public void run(){
        while (!client.isClosed()) {
            try{
                System.out.println("Message Receptor established"); /*connection established!*/
                String msg;
                while(!clientChat.exit) {
                    if (in.available() != 0) {
                        // Récupère le message envoyé par le serveur
                        msg = in.readUTF();
                        // Affichage du message sur la console
                        System.out.println(msg);
                        if ("Serveur: Vous etes bien déconnecté !".equals(msg)) {
                            disconnection();
                            System.out.println("Client Receptor end");
                        }
                    }
                }
            } catch(SocketException socketException) {
                clientChat.exit = true;
                System.out.println("Au revoir !");
            } catch (IOException ex) {
                Logger.getLogger(ClientChat.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    /**
     *treat the offline of the client
     * @throws IOException
     */
    private void disconnection() throws IOException {
        in.close();
        client.shutdownInput();
    }
}
