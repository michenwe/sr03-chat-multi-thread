import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;


public class ClientHeart extends Thread{
    private Socket client;
    private ClientChat clientChat;

    ClientHeart(Socket client,ClientChat clientChat){
        this.client = client;
        this.clientChat = clientChat;
    }

    @Override
    public void run() {
        try {
            System.out.println("Démarrage de Heartbeat...");
            while (!clientChat.exit){
                Thread.sleep(5000);
                //System.out.println("Heartbeat...");
            }
        }catch (Exception ex1){
            Logger.getLogger(ClientChat.class.getName()).log(Level.SEVERE, null, ex1);
            try {
                client.close();
                clientChat.connectionState = false;
                clientChat.reconnect(clientChat);
            }catch (Exception ex2){
                Logger.getLogger(ClientChat.class.getName()).log(Level.SEVERE, null, ex2);
            }
        }
    }
}
