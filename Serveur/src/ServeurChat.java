import java.io.*;
import java.net.Socket;
import java.net.ServerSocket;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.concurrent.ConcurrentHashMap;
import java.util.Map;

/**
 * Cette classe est le serveur de notre programme
 * Elle contient un ConcurrentHashMap qui enregistre tous les MessageReceptors de clients et évite le doublon de pseudo
 */
public class ServeurChat {
    private static Map<String, Socket> myClients = new ConcurrentHashMap<>();
    public static void main(String[] args) {
        try {
            // initialisation de serveur et socket
            ServeurChat myServ = new ServeurChat();
            System.out.println("Server start!");
            ServerSocket conn = new ServerSocket(20001);
            while (true){
                // attendre le nouveau client à connecter, s'il y a un, récupère le socket
                Socket comm = conn.accept();
                System.out.println("Nouveau Client...");
                MessageReceptor msg_receptor = new MessageReceptor(comm,myServ);
                msg_receptor.start();
            }
        } catch (IOException ex) {
            Logger.getLogger(ServeurChat.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    /**
     * Cette méthode ajoute un client dans le concurrentHashMap myClients
     * @param pseudo,MessageRecepteur qui sont respectivement key et value de myClients
     */
    public void AddClient(String pseudo, Socket client) {
        myClients.put(pseudo, client);
    }
    /**
     * Cette méthode efface le client dans le concurrentHashMap myClients
     * @param pseudo identifient de client
     */
    public void RemoveClient(String pseudo){myClients.remove(pseudo);}
    /**
     * Diffuse le message envoyé par un client aux les autres
     * @param msg le message à envoyer
     * @throws IOException
     */
    public void BroadCast(String msg) throws IOException {
        System.out.println(msg);
        Iterator<Map.Entry<String, Socket>> it = myClients.entrySet().iterator();
        Socket current_client;
        while (it.hasNext()) {
            Map.Entry<String, Socket> entry = it.next();
            current_client = entry.getValue();
            DataOutputStream out = new DataOutputStream(current_client.getOutputStream());
            out.writeUTF(msg);
        }
    }
    /**
     * Distinguer si le pseudo de nouveau client à connecter existe ou pas
     * @param pseudo identifient de client
     * @return pseudo de client existe ou pas
     */
    public static boolean pseudoIsUsed(String pseudo) {
        return myClients.containsKey(pseudo);
    }
}
