import java.net.Socket;
import java.io.IOException;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Cette classe est pour l'objectif de traiter le connection, le communication et le disconnection d'un client
 * Elle contient un socket, DataInputStream et DataOutputStream qui nous permet de communiquer avec le client
 * serv est le serveur de ce MessageReceptor
 * pseudo est l'identifient de client
 * exit qui signifie que si le client est déconnecté ou pas
 */
public class MessageReceptor extends Thread {
    private Socket socketClient;
    private ServeurChat serv;
    DataInputStream in;
    DataOutputStream out;
    private String pseudo;
    private String msg;
    private boolean exit = false;

    public MessageReceptor(Socket client,ServeurChat myServ) throws IOException {
        this.socketClient = client;
        this.serv = myServ;
        this.out = new DataOutputStream(client.getOutputStream());
        this.in = new DataInputStream(client.getInputStream());
    }
    @Override
    public void run(){
        try{
            this.pseudo = this.in.readUTF();
            while (this.serv.pseudoIsUsed(this.pseudo)) {
                this.out.writeUTF("Erreur: Pseudo déjà utilisé !");
                this.out.writeUTF("Veuillez entrer un autre pseudo, s'il vous plait !");
                this.pseudo = this.in.readUTF();
            }
            synchronized (this) {
                this.serv.AddClient(this.pseudo, this.socketClient);
            }
            //System.out.println(this.pseudo + " est connecté!"); /*connection established!*/
            serv.BroadCast(this.pseudo + " a rejoint la conversation"); /*connection established!*/
            out.writeUTF("Serveur: Bienvenue " + pseudo);
            out.writeUTF("Serveur: Si vous voulez quitter la discussion, veuillez taper 'exit'");
            out.writeUTF("-----------------------------------");
            while (!exit) {
                // récupère le msg envoyé par le client
                msg = in.readUTF();
                if (msg.equals("exit")) {
                    System.out.println("entered exit");
                    exit = true;
                    disconnection();
                } else {
                    // BroadCast le msg aux les autres clients
                    msg = pseudo + " a dit : " + msg;
                    serv.BroadCast(msg);
                }
            }
        }catch(IOException ex){
            // Le moment qu'on tombe en panne, on lance le disconnexion pour assuer le fermeture de socket
            try {
                disconnection();
            } catch (IOException e) {
                Logger.getLogger(MessageReceptor.class.getName()).log(Level.SEVERE, null, e);
            }
            System.out.println("Déconnexion de " + pseudo + " à cause d'une probleme :(");
            Logger.getLogger(MessageReceptor.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Fermer le DataOutputStream, DataInputStream et socket et informer le serveur d'effacer le client
     * @throws IOException
     */
    private void disconnection() throws IOException {
        synchronized (this) {
            serv.RemoveClient(pseudo);
        }
        out.writeUTF("Serveur: Vous etes bien déconnecté !");
        serv.BroadCast(pseudo + " est déconnecté !");
        in.close();
        out.close();
        socketClient.close();
        System.out.println(pseudo+ " est bien déconnecté !");
    }
}
